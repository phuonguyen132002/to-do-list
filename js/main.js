import { callApi } from "./callapi.js";
import Task from "../models/task.js";

const showTaskList = () => {
  document.getElementById("loader").style.display = "block";
  callApi("task", "GET", null)
    .then((res) => {
      document.getElementById("loader").style.display = "none";
      sortTaskStatus(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
showTaskList();

const renderTable = (arr) => {
  let content = "";
  arr.forEach((task) => {
    content += `
        <li>
            <span>${task.txtToDo}</span>
            <div class="buttons">
                <button onclick="deleteTask(${task.id})"><i class="fa fa-trash-alt"></i></button>
                <button onclick="getTaskByID(${task.id})"><i class="fa fa-check"></i></button>
            </div>
        </li>

      `;
  });
  return content;
};

const sortTaskStatus = (arr) => {
  let completedTask = [];
  let incompleteTask = [];
  arr.forEach((task) => {
    if (task.status === "to-do") {
      incompleteTask.push(task);
    } else if (task.status === "completed") {
      completedTask.push(task);
    }
  });

  document.getElementById("todo").innerHTML = renderTable(incompleteTask);
  document.getElementById("completed").innerHTML = renderTable(completedTask);
};

document.getElementById("addItem").addEventListener("click", () => {
  const inpTask = document.getElementById("newTask").value;

  const task = new Task("", inpTask, "to-do");
  callApi("task", "POST", task)
    .then(() => {
      document.getElementById("newTask").value = "";
      showTaskList();
    })
    .catch((err) => {
      console.log(err);
    });
});

const deleteTask = (id) => {
  callApi(`task/${id}`, "DELETE", null)
    .then(() => {
      showTaskList();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteTask = deleteTask;

const getTaskByID = (id) => {
  callApi(`task/${id}`, "GET", null)
    .then((res) => {
      changeTaskStatus(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.getTaskByID = getTaskByID;

const changeTaskStatus = (task) => {
  if (task.status === "to-do") {
    task.status = "completed";

    updateTask(task);
  } else {
    task.status = "to-do";
    updateTask(task);
  }
  showTaskList();
};

const updateTask = (task) => {
  callApi(`task/${task.id}`, "PUT", task)
    .then(() => {
      showTaskList();
    })
    .catch((err) => {
      console.log(err);
    });
};
