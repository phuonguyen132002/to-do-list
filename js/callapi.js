import { API_URL } from "./../configs/constants.js";
const callApi = (uri, method, data) => {
  return axios({
    url: `${API_URL}/${uri}`,
    method: method,
    data: data,
  });
};
export { callApi };
