class Task {
  constructor(_id, _txtToDo, _status) {
    this.id = _id;
    this.txtToDo = _txtToDo;
    this.status = _status;
  }
}
export default Task;
